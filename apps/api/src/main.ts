import { NestFactory } from '@nestjs/core';
import { ApiModule } from './api.module';

async function bootstrap() {
	const app = await NestFactory.create(ApiModule);
	app.enableCors();
	const port = process.env.port || 3000;
	await app.listen(port);
	console.log(`The app is running on port ${port}`);
	
}
bootstrap();
